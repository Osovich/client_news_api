Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #post auth/login pasandole email y password te da el token
  #a partir de aquí todo requiere token 
  #post /users/:user_id/favorites crea favoritos
  #get /favorites/id muestra una noticia
  #get /news requiere una url de reddit o digg
  #get /news/pick requiere de una url actual en el caso de reddit, en dig solo sería url
  resources :users, only: [:index, :create, :update], shallow: true do
    resources :favorites, only: [:index, :show, :create]
  end
  post 'auth/login', to: 'sessions#create'
  get 'news/pick', to: 'news#show'
  get 'news', to: 'news#index'
end
