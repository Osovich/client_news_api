class NewsController < ApplicationController
    include Reddit
    include Digg
  
    def index
      if params[:url] == "https://www.reddit.com/.json"
        if params[:direction].blank? or ((!params[:direction].blank? and params[:before].blank?) and (!params[:direction].blank? and params[:after].blank?))
            hash = reddit_news_list(params[:url])
        elsif params[:direction] == "0" and !params[:before].blank?
            hash = reddit_news_list(params[:url], "before", params[:before])
        elsif params[:direction] == "1" and !params[:after].blank?
            hash = reddit_news_list(params[:url], "after", params[:after])
        end
        render json: hash
      elsif params[:url] == "http://digg.com/api/news/popular.json"
        hash = digg_news_list(params[:url])
        render json: hash
      end
    end
    
    def show
        if params[:url] == "https://www.reddit.com/.json"
            hash = reddit_news_pick(params[:current_url], params[:news_id])
            render json: hash
        elsif params[:url] == "http://digg.com/api/news/popular.json"
            hash = digg_news_pick(params[:url], params[:news_id])
            render json: hash
        end
    end
    private
  
    def new_params
      params.permit(:before, :after, :direction, :url, :current_url, :news_id)
    end
  end