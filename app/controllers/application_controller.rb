class ApplicationController < ActionController::API
  before_action :authenticate!

  SECRET = Rails.application.secrets.secret_key_base

  private

  def authenticate!
    begin
      token = request.headers[:Authorization]
      data = JWT.decode(token, SECRET).first
      @current_user = User.find(data['user_id'])
    rescue
      # byebug
      render json: { error: 'Access Denied' }, status: :unauthorized
    end
  end
end
