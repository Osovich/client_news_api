class UsersController < ApplicationController
  skip_before_action :authenticate!, only: :create

  def index
    users = User.all
    render json: users
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user
    else
      render json: user.errors.messages, status: :bad_request
    end
  end

  def update
    user = User.find(params[:id])
    if user == @current_user
      if user.update(user_params)
        render json: user
      else
        render json: user.errors.messages
      end
    else
      render json: { error: 'Unauthorized' }, status: :unauthorized
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
