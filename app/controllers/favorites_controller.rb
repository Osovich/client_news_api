class FavoritesController < ApplicationController
    before_action :set_favorite, only: :show

    def index
        @favorites = Favorite.all
        render json: @favorites
    end

    def show
        render json: @favorite, serializer: FullFavoriteSerializer
    end
  
    def create
        @user = User.find_by(id: params[:user_id])
        if @user = @current_user
            @favorite = @user.favorites.build(favorite_params)
            if @favorite.save
                render json: @favorite
            else
                render json: @favorite.errors.messages, status: :bad_request
            end
        else
            render json: {error: "Usuario no es"}
        end
    end
  
    private

    def set_favorite
        @favorite = Favorite.find(params[:id])
    end

  
    def favorite_params
        params.permit(:title, :author, :description, :date, :category, :website, :url)
    end
  end
  