class FullFavoriteSerializer < ActiveModel::Serializer
    attributes :title, :author, :description, :date, :category, :website, :url
end