class FavoriteSerializer < ActiveModel::Serializer
    attributes :id, :title, :author, :date, :category, :website, :url
end