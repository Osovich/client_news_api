class DropCategoriesAndWebsites < ActiveRecord::Migration[5.2]
  def change
    drop_table :categories do |t|
      t.string :name
    end
    drop_table :websites do |t|
      t.string :name
    end
  end
end
