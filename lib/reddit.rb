module Reddit
    require 'rest-client'
    require 'json'

    def reddit_news_list(url, key = "", direction = "")
        hash = { data: [], before: nil, after: nil, current_url: "" }
        if !key.blank? and !direction.blank?
            hash[:current_url] = url + "?" + key.to_s + "=" + direction.to_s
            url = url + "?" + "limit" + "=" + "5" + "&" + key.to_s + "=" + direction.to_s + "&" + "count" + "=" + "5"
        else
            hash[:current_url] = url
            url = url + "?" + "limit" + "=" + "5"
        end
        response = RestClient.get url
        data = JSON.parse response

        #Llenamos el hash
        hash[:before] = data['data']['before']
        hash[:after] = data['data']['after']
        data['data']['children'].each do |single|
            sing_new = { title: "",
                    date: "",
                    author: "",
                    category: "",
                    url: "",
                    news_id: "" 
                    }
            sing_new[:title] = single['data']['title']
            sing_new[:date] = (Time.at(single['data']['created_utc'])).strftime("%d/%m/%Y")
            sing_new[:author] = single['data']['author']
            sing_new[:category] = single['data']['subreddit_name_prefixed']
            sing_new[:url] = single['data']['url']
            sing_new[:news_id] = single['data']['id']
            hash[:data] << sing_new
        end
        hash
    end

    def reddit_news_pick(current_url, news_id)
        hash = { data: [], current_url: "" }
        sing_new = { title: "",
                        description: "",
                        date: "",
                        author: "",
                        category: "",
                        url: "",
                        news_id: "" 
                    }

        response = RestClient.get current_url
        data = JSON.parse response

        #Llenamos el hash
        hash[:current_url] = current_url
        data['data']['children'].each do |single|
            if single['data']['id'] == news_id
                sing_new[:title] = single['data']['title']
                sing_new[:description] = (single['data']['selftext']).gsub("\n",'')
                sing_new[:date] = (Time.at(single['data']['created_utc'])).strftime("%d/%m/%Y")
                sing_new[:author] = single['data']['author']
                sing_new[:category] = single['data']['subreddit_name_prefixed']
                sing_new[:url] = single['data']['url']
                sing_new[:news_id] = single['data']['id']
                hash[:data] << sing_new
                break
            end
        end
        hash
    end
end