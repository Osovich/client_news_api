#require_relative 'main.rb'
require 'io/console'
require 'rest-client'
require 'json'
require "colorize"

    def key
        system ("clear")
        puts "
        
                                                LL      EEEEEEE  CCCCC  TTTTTTT  OOOOO  RRRRRR     RRRRRR   SSSSS   SSSSS  
                                                LL      EE      CC    C   TTT   OO   OO RR   RR    RR   RR SS      SS      
                                                LL      EEEEE   CC        TTT   OO   OO RRRRRR     RRRRRR   SSSSS   SSSSS  
                                                LL      EE      CC    C   TTT   OO   OO RR  RR     RR  RR       SS      SS 
                                                LLLLLLL EEEEEEE  CCCCC    TTT    OOOO0  RR   RR    RR   RR  SSSSS   SSSSS  
                                                                                
                                                                                            
        "
        print "                                                                                press any key"                                                                                                    
        STDIN.getch                                                                                                              
        # extra space to overwrite in case next sentence is short                                                                                                              
        i = 1
        load = "*"
        colors = [:green, :blue, :yellow, :red, :light_blue, :magenta, :gray]
        colors2 = [:green, :blue, :yellow, :red, :light_blue, :magenta, :gray]
        puts 
        puts   
        50.times do
            color = colors[rand(0..6)]
            back = colors2[rand(0..6)]
            system ("clear")
            puts
            puts 
            puts 
            puts  "  #{load}".colorize(color).colorize( :background => back)*i
            puts " 

                    LLLLLLLLLLL                       OOOOOOOOO                         AAA                    DDDDDDDDDDDDD        
                    L:::::::::L                     OO:::::::::OO                      A:::A                   D::::::::::::DDD     
                    L:::::::::L                   OO:::::::::::::OO                   A:::::A                  D:::::::::::::::DD   
                    LL:::::::LL                  O:::::::OOO:::::::O                 A:::::::A                 DDD:::::DDDDD:::::D  
                    L:::::L                      O::::::O   O::::::O                A:::::::::A                  D:::::D    D:::::D 
                    L:::::L                      O:::::O     O:::::O               A:::::A:::::A                 D:::::D     D:::::D
                    L:::::L                      O:::::O     O:::::O              A:::::A A:::::A                D:::::D     D:::::D
                    L:::::L                      O:::::O     O:::::O             A:::::A   A:::::A               D:::::D     D:::::D
                    L:::::L                      O:::::O     O:::::O            A:::::A     A:::::A              D:::::D     D:::::D
                    L:::::L                      O:::::O     O:::::O           A:::::AAAAAAAAA:::::A             D:::::D     D:::::D
                    L:::::L                      O:::::O     O:::::O          A:::::::::::::::::::::A            D:::::D     D:::::D
                    L:::::L          LLLLLL      O::::::O   O::::::O         A:::::AAAAAAAAAAAAA:::::A           D:::::D    D:::::D 
                    LL:::::::LLLLLLLLL:::::L     O:::::::OOO:::::::O        A:::::A             A:::::A        DDD:::::DDDDD:::::D  
                    L::::::::::::::::::::::L      OO:::::::::::::OO        A:::::A               A:::::A       D:::::::::::::::DD   
                    L::::::::::::::::::::::L        OO:::::::::OO         A:::::A                 A:::::A      D::::::::::::DDD     
                    LLLLLLLLLLLLLLLLLLLLLLLL          OOOOOOOOO          AAAAAAA                   AAAAAAA     DDDDDDDDDDDDD        ".colorize(:red )
            
            puts
            puts  "  #{load}".colorize(color).colorize( :background => back)*i
            i+=1
        end 
    end

    def log
        key
        loop do
            reddit = 'https://www.reddit.com/.json'
            digg = 'http://digg.com/api/news/popular.json'
            system('clear')
            puts "1 to Login"
            puts "2 to Sign In"
            puts "3 to Exit"
            intro = gets.chomp().to_i
            if intro == 1
                puts "Login, b to go back, enter to continue"
                b = gets.chomp
                if b != "b"
                    token = ""
                    data = ""
                    response = ""
                    loop do
                        bien = true
                        email = ""
                        password = ""
                        loop do
                            puts "Enter Email"
                            email = gets.chomp()
                            puts "Enter Password"
                            password = STDIN.noecho(&:gets).chomp
                        break if(credentials(email, password))
                        end
                        begin
                            response = RestClient.post "localhost:3000/auth/login", { user: { email: email,
                                                                        password: password } }
                        rescue => e
                            puts e.message
                            bien = false
                        end
                    break if(bien)
                    end
                    data = JSON.parse response
                    token = data['token']
                    user_id = data['user_id'].to_s
                    loop do
                        system('clear')
                        puts "1 for Reddit, 2 for Digg, 3 for favorites, s to go back"
                        site = gets.chomp
                        response = ""
                        if site.to_i == 1
                            response = RestClient.get "localhost:3000/news", {Authorization: token , params:
                                                                             { url: reddit } }
                            loop do
                                system('clear')
                                data = JSON.parse response
                                index_news(data)
                                puts "'s' to go back, 'a' for previous news, 'd' for next news, or enter new's index"
                                pick = gets.chomp
                                if pick == "a"
                                    #data['data'][pick]
                                    response = RestClient.get "localhost:3000/news", {Authorization: token , params: 
                                                                                        { url: reddit, before: data['before'], direction: '0' } }
                                elsif pick == "d"
                                    response = RestClient.get "localhost:3000/news", {Authorization: token , params:
                                                                                        { url: reddit , after: data['after'], direction: '1' } }
                                elsif pick.to_i >= 1 and pick.to_i <= 5
                                    sing_response = RestClient.get "localhost:3000/news/pick", {Authorization: token, params:
                                                                                        { url: reddit , current_url: data['current_url'], news_id: data['data'][pick.to_i-1]['news_id'] } }
                                    sing_data = JSON.parse sing_response
                                    show_new(sing_data)
                                    puts "f to save as favorite, o to open in browser"
                                    s_input = gets.chomp
                                    if s_input == "f"
                                        fav = "localhost:3000/users/" + user_id + "/favorites"
                                        RestClient.post(fav, {  website: reddit , title: sing_data['data'][0]['title'],
                                                                author: sing_data['data'][0]['author'],
                                                                description: sing_data['data'][0]['description'],
                                                                date: sing_data['data'][0]['date'],
                                                                category: sing_data['data'][0]['category'],
                                                                url: sing_data['data'][0]['url'] },
                                                                Authorization: token )
                                    puts "Saved as Favorite"
                                    gets.chomp
                                    elsif s_input == "o"
                                        system "xdg-open #{sing_data['data'][0]['url']}"
                                    end
                                end
                            break if(pick == "s")
                            end
                        elsif site.to_i == 2
                            response = RestClient.get "localhost:3000/news", {Authorization: token , params: { url: digg } }
                            loop do
                                system('clear')
                                data = JSON.parse response
                                index_news(data)
                                puts "'s' to go back, or input new's index"
                                pick = gets.chomp
                                if pick.to_i >= 1 and pick.to_i <= data['data'].length
                                    sing_response = RestClient.get "localhost:3000/news/pick", {Authorization: token, params:
                                                                                        { url: digg , current_url: data['current_url'], news_id: data['data'][pick.to_i-1]['news_id'] } }
                                    sing_data = JSON.parse sing_response
                                    show_new(sing_data)
                                    puts "f to save as favorite"
                                    s_input = gets.chomp
                                    if s_input == "f"
                                        fav = "localhost:3000/users/" + user_id + "/favorites"
                                        RestClient.post(fav, {  website: reddit , title: sing_data['data'][0]['title'],
                                                                author: sing_data['data'][0]['author'],
                                                                description: sing_data['data'][0]['description'],
                                                                date: sing_data['data'][0]['date'],
                                                                category: sing_data['data'][0]['category'],
                                                                url: sing_data['data'][0]['url'] },
                                                                Authorization: token )
                                    puts "Saved as Favorite"
                                    gets.chomp
                                    end
                                    system('clear')
                                end
                            break if(pick == "s")
                            end
                        elsif site.to_i == 3
                            fav = "localhost:3000/users/" + user_id + "/favorites"
                            response = RestClient.get fav, { Authorization: token }
                            loop do
                                data = JSON.parse response
                                if data.empty?
                                    puts "You have no favorites"
                                    gets.chomp
                                    break
                                end
                                index_favs(data)
                                puts "s to go back, or input new's index"
                                pick = gets.chomp
                                if pick.to_i >= 1 and pick.to_i <= data.length
                                    sing_response = RestClient.get "localhost:3000/favorites/" + pick.to_s, { Authorization: token }
                                    sing_data = JSON.parse sing_response
                                    show_fav(sing_data)
                                    puts "o to open in browser"
                                    o = gets.chomp
                                    if (o == "o")
                                        system "xdg-open #{sing_data['url']}"
                                    end
                                end
                            break if(pick == "s")
                            end
                        end
                    break if(site == "s")
                    end
                end
            elsif intro == 2
                loop do
                    puts "Sign In, b to go back, any other key to continue"
                    b = gets.chomp
                    if b != "b"
                        email = ""
                        password = ""
                        loop do
                            puts "Enter Email"
                            email = gets.chomp()
                            puts "Enter Password"
                            password = STDIN.noecho(&:gets).chomp
                        break if(credentials(email, password))
                        end
                        begin
                            RestClient.post "localhost:3000/users", { user: { email: email,
                                                                        password: password } } 
                        rescue
                            puts "Email in use"
                            gets.chomp
                        end
                    end
                break
                end
            end
        break if(intro == 3)
        end
    end

    def valid_email?(email)
        valid_email_regex = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
        if (email =~ valid_email_regex) == nil
            return false
        else
            return true
        end
    end

    def credentials(email, password)
        if email.empty? or !valid_email?(email) or password.empty?
            puts "Invalid credentials"
            return false
        end
        return true
    end

    def show_new(sing_data)
        system('clear')
        puts
        puts "Title = " + sing_data['data'][0]['title']
        puts "Description = " + sing_data['data'][0]['description']
        puts "Date = " + sing_data['data'][0]['date']
        puts "Author = " + sing_data['data'][0]['author']
        puts "Category = " + sing_data['data'][0]['category']
        puts "URL = " + sing_data['data'][0]['url']
        puts
    end

    def show_fav(sing_data)
        system('clear')
        puts
        puts "Title = " + sing_data['title']
        puts "Description = " + sing_data['description']
        puts "Date = " + sing_data['date']
        puts "Author = " + sing_data['author']
        puts "Category = " + sing_data['category']
        puts "URL = " + sing_data['url']
        puts
    end

    def index_news(data)
        data['data'].each_with_index do |t, index|
            puts
            puts index+1
            puts "Title = " + t['title']
            puts "Date = " + t['date']
            puts "Author = " + t['author']
            puts "Category = " + t['category']
            puts "URL = " + t['url']
            puts
        end
    end

    def index_favs(data)
        data.each_with_index do |t, index|
            puts
            puts index+1
            puts "Title = " + t['title']
            puts "Date = " + t['date']
            puts "Author = " + t['author']
            puts "Category = " + t['category']
            puts "URL = " + t['url']
            puts
        end
    end
log