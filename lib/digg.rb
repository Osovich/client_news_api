module Digg
    require 'rest-client'
    require 'json'

    def digg_news_list(url, key = "", direction = "")
        hash = { data: [], before: nil, after: nil, current_url: "" }

        response = RestClient.get url
        data = JSON.parse response

        data['data']['feed'].each do |single|
            sing_new = { title: "",
                    date: "",
                    author: "",
                    category: "",
                    url: "",
                    news_id: "" 
                    }
            sing_new[:title] = single['content']['title']
            sing_new[:date] = (Time.at(single['date_published'])).strftime("%d/%m/%Y")
            sing_new[:author] = single['content']['author']
            sing_new[:category] = single['content']['tags'][0]['display_name']
            sing_new[:url] = single['content']['url']
            sing_new[:news_id] = single['story_id']
            hash[:data] << sing_new
        end
        hash
    end

    def digg_news_pick(url, news_id)
        hash = { data: [] }

        response = RestClient.get url
        data = JSON.parse response

        data['data']['feed'].each do |single|
            if single['story_id'] == news_id
                sing_new = { title: "",
                        description: "",
                        date: "",
                        author: "",
                        category: "",
                        url: "",
                        news_id: "" 
                        }
                sing_new[:title] = single['content']['title']
                sing_new[:description] = (single['content']['description']).gsub("\n",'')
                sing_new[:date] = (Time.at(single['date_published'])).strftime("%d/%m/%Y")
                sing_new[:author] = single['content']['author']
                sing_new[:category] = single['content']['tags'][0]['display_name']
                sing_new[:url] = single['content']['url']
                sing_new[:news_id] = single['story_id']
                hash[:data] << sing_new
                break
            end
        end
        hash
    end
end