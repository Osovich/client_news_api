class CreateFavorites < ActiveRecord::Migration[5.2]
  def change
    create_table :favorites do |t|
      t.string :url
      t.string :title
      t.string :description
      t.date :date
      t.string :author
      t.references :category, foreign_key: true
      t.references :website, foreign_key: true

      t.timestamps
    end
  end
end
