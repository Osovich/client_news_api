class ModifyFavorites < ActiveRecord::Migration[5.2]
  def change
    remove_column :favorites, :category_id
    remove_column :favorites, :website_id
    add_column :favorites, :category, :string
    add_column :favorites, :website, :string
  end
end
