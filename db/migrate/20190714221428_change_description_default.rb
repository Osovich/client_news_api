class ChangeDescriptionDefault < ActiveRecord::Migration[5.2]
  def change
    change_column_default :favorites, :description, from: nil, to: ""
  end
end
