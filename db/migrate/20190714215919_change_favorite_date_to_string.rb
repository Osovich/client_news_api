class ChangeFavoriteDateToString < ActiveRecord::Migration[5.2]
  def change
    remove_column :favorites, :date, :date
    add_column :favorites, :date, :string
  end
end
